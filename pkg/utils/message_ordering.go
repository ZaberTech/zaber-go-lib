package utils

import (
	"fmt"
	"sync"
	"time"
)

const maxOutOfOrderMessages = 100

type message[T interface{}] struct {
	data T
	id   uint32
}

type MessageOrdering[T interface{}] struct {
	lock         sync.Mutex
	isRunning    bool
	nextID       uint32
	orderedData  chan T
	messages     chan *message[T]
	stored       []*message[T]
	timeout      time.Duration
	timeoutFunc  func()
	timeoutTimer *time.Timer
	stopped      chan interface{}
	shouldStop   chan interface{}
}

func NewMessageOrdering[T interface{}](timeout time.Duration, timeoutFunc func()) *MessageOrdering[T] {
	box := &MessageOrdering[T]{
		orderedData: make(chan T, 10),
		messages:    make(chan *message[T]),
		timeout:     timeout,
		timeoutFunc: timeoutFunc,
	}

	return box
}

func (box *MessageOrdering[T]) Write(data T, id uint32) error {
	box.lock.Lock()
	stopped := box.stopped
	isRunning := box.isRunning
	box.lock.Unlock()

	if !isRunning {
		return fmt.Errorf("Unexpected write")
	}

	select {
	case box.messages <- &message[T]{
		data: data,
		id:   id,
	}:
		return nil
	case <-stopped:
		return fmt.Errorf("Stopped")
	}
}

func (box *MessageOrdering[T]) OrderedData() chan T {
	return box.orderedData
}

func (box *MessageOrdering[T]) processMessage(message *message[T]) {
	idDiff := (int32)(message.id - box.nextID)
	if idDiff == 0 {
		box.dispatchMessage(message)
	} else if idDiff < 0 { // duplicate
		return
	} else { // out of order
		box.stored = append(box.stored, message)
	}
}

func (box *MessageOrdering[T]) dispatchMessage(message *message[T]) {
	box.nextID = message.id + 1
	select {
	case box.orderedData <- message.data:
	case <-box.shouldStop:
	}
}

func (box *MessageOrdering[T]) processStored() {
	for i := 0; i < len(box.stored); i++ {
		message := box.stored[i]

		if message.id == box.nextID {
			box.dispatchMessage(message)

			box.stored = append(box.stored[:i], box.stored[i+1:]...)
			i = -1
		}
	}
}

func (box *MessageOrdering[T]) loop() {
	defer close(box.stopped)
	defer box.ensureTimeoutStopped()
	box.nextID = 0
	box.stored = nil

	for {
		if len(box.stored) == 0 {
			box.ensureTimeoutStopped()
			select {
			case message := <-box.messages:
				box.processMessage(message)
				box.processStored()
			case <-box.shouldStop:
				return
			}
		} else {
			box.ensureTimeoutStarted()
			select {
			case message := <-box.messages:
				box.processMessage(message)
				box.processStored()
			case <-box.timeoutTimer.C:
				box.timeoutFunc()
				return
			case <-box.shouldStop:
				return
			}

			if len(box.stored) >= maxOutOfOrderMessages {
				box.timeoutFunc()
				return
			}
		}
	}
}

func (box *MessageOrdering[T]) stop(startAgain bool) {
	box.lock.Lock()
	defer box.lock.Unlock()

	if box.isRunning {
		close(box.shouldStop)
		<-box.stopped
		box.isRunning = false
	}

	if startAgain {
		box.isRunning = true
		box.shouldStop = make(chan interface{})
		box.stopped = make(chan interface{})

		go box.loop()
	}
}

func (box *MessageOrdering[T]) Stop() {
	box.stop(false)
}

func (box *MessageOrdering[T]) Reset() {
	box.stop(true)
}

func (box *MessageOrdering[T]) ensureTimeoutStarted() {
	if box.timeoutTimer == nil {
		box.timeoutTimer = time.NewTimer(box.timeout)
	}
}

func (box *MessageOrdering[T]) ensureTimeoutStopped() {
	if box.timeoutTimer != nil {
		box.timeoutTimer.Stop()
		box.timeoutTimer = nil
	}
}
