package utils

import "testing"

func TestVersion_Parse(t *testing.T) {
	fw, err := ParseVersion("1.2")
	if err != nil || fw != (Version{1, 2}) {
		t.Error("ParseFWVersion(1.2)", err)
	}
}

func TestVersion_Gte(t *testing.T) {
	if VersionGte(Version{1, 2}, Version{1, 2}) != true {
		t.Error("VersionGte")
	}
	if VersionGte(Version{1, 3}, Version{1, 2}) != true {
		t.Error("VersionGte")
	}
	if VersionGte(Version{2, 0}, Version{1, 2}) != true {
		t.Error("VersionGte")
	}
	if VersionGte(Version{1, 1}, Version{1, 2}) != false {
		t.Error("VersionGte")
	}
	if VersionGte(Version{2, 5}, Version{3, 1}) != false {
		t.Error("VersionGte")
	}
}

func TestVersion_Compatible(t *testing.T) {
	if VersionCompatible(Version{1, 2}, Version{1, 2}) != true {
		t.Error("VersionCompatible")
	}
	if VersionCompatible(Version{1, 3}, Version{1, 2}) != true {
		t.Error("VersionCompatible")
	}
	if VersionCompatible(Version{1, 1}, Version{1, 2}) != false {
		t.Error("VersionCompatible")
	}
	if VersionCompatible(Version{2, 0}, Version{1, 2}) != false {
		t.Error("VersionCompatible")
	}
}
