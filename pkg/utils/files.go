package utils

import "os"

func IsValidDirectory(dir string) bool {
	if dir == "" {
		return false
	}
	stat, err := os.Stat(dir)
	return err == nil && stat.IsDir()
}
