package utils

import (
	"fmt"
	"strconv"
	"strings"
)

type Version struct {
	Major int
	Minor int
}

func ParseVersion(versionStr string) (Version, error) {
	version := Version{}
	parts := strings.Split(versionStr, ".")
	if len(parts) != 2 {
		return version, fmt.Errorf("cannot parse version %s", versionStr)
	}
	if Major, err := strconv.Atoi(parts[0]); err != nil {
		return version, err
	} else { //revive:disable-line:indent-error-flow
		version.Major = Major
	}
	if Minor, err := strconv.Atoi(parts[1]); err != nil {
		return version, err
	} else { //revive:disable-line:indent-error-flow
		version.Minor = Minor
	}
	return version, nil
}

func (version Version) String() string {
	return fmt.Sprintf("%d.%d", version.Major, version.Minor)
}

// v1 >= v2
func VersionGte(v1 Version, v2 Version) bool {
	return v1.Major > v2.Major || (v1.Major == v2.Major && v1.Minor >= v2.Minor)
}

// version ∈ ^required
func VersionCompatible(version Version, required Version) bool {
	return version.Major == required.Major && version.Minor >= required.Minor
}
