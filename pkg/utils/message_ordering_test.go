package utils

import (
	"fmt"
	"testing"
	"time"

	"gotest.tools/assert"
)

var messageOrdering *MessageOrdering[string]
var timeout chan bool

func setupTestCase(t *testing.T) func() {
	timeout = make(chan bool, 1)
	messageOrdering = NewMessageOrdering[string](100*time.Millisecond, func() {
		timeout <- true
	})
	messageOrdering.Reset()
	return func() {
		messageOrdering.Stop()
		messageOrdering = nil

		if timeout != nil {
			close(timeout)
			timeout = nil
		}
	}
}

// orders messages and deals with duplicates
func TestMessageOrdering_ordersMessages(t *testing.T) {
	teardown := setupTestCase(t)
	defer teardown()

	go func() {
		_ = messageOrdering.Write("1", 1)
		_ = messageOrdering.Write("3", 3)
		_ = messageOrdering.Write("0", 0)
		_ = messageOrdering.Write("2", 2)
		_ = messageOrdering.Write("5", 5)
		_ = messageOrdering.Write("2", 2)
		_ = messageOrdering.Write("4", 4)
	}()

	for i := 0; i <= 5; i++ {
		line := <-messageOrdering.OrderedData()
		assert.Equal(t, line, fmt.Sprintf("%d", i))
	}
}

// missing packet causes timeout
func TestMessageOrdering_missingPacket(t *testing.T) {
	teardown := setupTestCase(t)
	defer teardown()

	_ = messageOrdering.Write("", 0)
	_ = messageOrdering.Write("", 1)

	select {
	case <-timeout:
		t.Fatal("Should not timeout")
	default:
	}

	_ = messageOrdering.Write("", 3)

	assert.Check(t, <-timeout)

	close(timeout)
	assert.Check(t, !(<-timeout), "There should be only one timeout")
	timeout = nil
}

func TestMessageOrdering_returnErrWhenStopped(t *testing.T) {
	teardown := setupTestCase(t)
	defer teardown()

	messageOrdering.Stop()

	assert.Error(t, messageOrdering.Write("", 0), "Unexpected write")
}

// reset starts the counting from zero
func TestMessageOrdering_allowsReset(t *testing.T) {
	teardown := setupTestCase(t)
	defer teardown()

	for i := uint32(0); i < 3; i++ {
		_ = messageOrdering.Write("", i)
		<-messageOrdering.OrderedData()
	}

	messageOrdering.Reset()

	for i := uint32(0); i < 3; i++ {
		_ = messageOrdering.Write("", i)
		<-messageOrdering.OrderedData()
	}
}
