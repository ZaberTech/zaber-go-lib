package communication

func GetMessageRouterPipe() (string, error) {
	return getPipeName("zaber-message-router", messageRouterPipeNameEnv)
}

func GetDbServicePipe() (string, error) {
	return getPipeName("zaber-db-service", dbServicePipeNameEnv)
}
