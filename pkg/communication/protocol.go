package communication

import (
	"strconv"
)

func VerifyLRC(packet string, claimedLRC string) bool {
	computedLRC := ComputeLRC(packet)
	parsedLRC, err := strconv.ParseInt(claimedLRC, 16, 0)
	return err == nil && parsedLRC == int64(computedLRC)
}

func ComputeLRC(packet string) byte {
	lcr := byte(0)
	for i := 1; i < len(packet); i++ {
		c := packet[i]

		if c == ':' {
			break
		}

		lcr -= c
	}
	return lcr
}
