//go:build !windows
// +build !windows

package communication

import (
	"errors"
	"fmt"
	"os"
	"os/user"
	"path/filepath"

	"gitlab.com/ZaberTech/zaber-go-lib/pkg/utils"
)

func getPipeName(pipeName, pipeNameEnv string) (string, error) {
	if pipe := os.Getenv(pipeNameEnv); pipe != "" {
		return pipe, nil
	}

	user, err := user.Current()
	if err != nil {
		return "", err
	}

	runtimeDir := os.Getenv("XDG_RUNTIME_DIR")
	if !utils.IsValidDirectory(runtimeDir) {
		runtimeDir = os.TempDir()
		if !utils.IsValidDirectory(runtimeDir) {
			return "", errors.New("Cannot find suitable runtime directory for the socket")
		}
	}

	return filepath.Join(runtimeDir, fmt.Sprintf("%s-%s.sock", pipeName, user.Uid)), nil
}
