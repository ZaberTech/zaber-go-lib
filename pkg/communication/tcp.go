// +build !wasm

package communication

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net"
	"strings"
	"sync"
)

type TCPConnection struct {
	connection net.Conn

	reader     *bufio.Reader
	readerLock sync.Mutex

	writerLock sync.Mutex

	lock     sync.Mutex
	isClosed bool
}

func OpenTCP(host string, port int) (*TCPConnection, CommErr) {
	dial := fmt.Sprintf("%s:%d", host, port)
	log.Print("Opening " + dial)

	netConnection, err := net.Dial("tcp", dial)
	if err != nil {
		return nil, NewErrConnectionFailed("Cannot establish tcp connection: " + err.Error())
	}

	connection := &TCPConnection{
		connection: netConnection,
		reader:     bufio.NewReader(netConnection),
	}

	return connection, nil
}

func (connection *TCPConnection) Close() CommErr {
	connection.lock.Lock()
	defer connection.lock.Unlock()

	if connection.isClosed {
		return nil
	}

	connection.isClosed = true

	err := connection.connection.Close()
	if err != nil {
		return NewErrConnectionFailed("Cannot close tcp connection: " + err.Error())
	}
	return nil
}

func (connection *TCPConnection) checkReadErrors(err error) CommErr {
	if err != nil {
		if connection.IsClosed() {
			return NewErrConnectionClosed()
		}
		return NewErrConnectionFailed("Cannot read from connection: " + err.Error())
	}

	return nil
}

func (connection *TCPConnection) ReadLine() (string, CommErr) {
	if connection.IsClosed() {
		return "", NewErrConnectionClosed()
	}

	connection.readerLock.Lock()
	defer connection.readerLock.Unlock()

	line, portErr := connection.reader.ReadString('\n')

	err := connection.checkReadErrors(portErr)
	if err != nil {
		return "", err
	}

	return strings.TrimSpace(line), nil
}

func (connection *TCPConnection) Read(num int) ([]byte, CommErr) {
	if connection.IsClosed() {
		return nil, NewErrConnectionClosed()
	}

	connection.readerLock.Lock()
	defer connection.readerLock.Unlock()

	packet := make([]byte, num)
	_, portErr := io.ReadFull(connection.reader, packet)

	err := connection.checkReadErrors(portErr)
	if err != nil {
		return nil, err
	}

	return packet, nil
}

func (connection *TCPConnection) WriteLines(lines []string) CommErr {
	if connection.IsClosed() {
		return NewErrConnectionClosed()
	}

	connection.writerLock.Lock()
	defer connection.writerLock.Unlock()

	var sb strings.Builder

	for _, line := range lines {
		sb.WriteString(line)
		sb.WriteString("\n")
	}

	_, err := connection.connection.Write([]byte(sb.String()))
	if err != nil {
		return NewErrConnectionFailed("Cannot write to connection: " + err.Error())
	}

	return nil
}

func (connection *TCPConnection) Write(packet []byte) CommErr {
	if connection.IsClosed() {
		return NewErrConnectionClosed()
	}

	connection.writerLock.Lock()
	defer connection.writerLock.Unlock()

	_, err := connection.connection.Write(packet)
	if err != nil {
		return NewErrConnectionFailed("Cannot write to connection: " + err.Error())
	}

	return nil
}

func (connection *TCPConnection) IsClosed() bool {
	connection.lock.Lock()
	defer connection.lock.Unlock()

	return connection.isClosed
}
