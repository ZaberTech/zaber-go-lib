package communication

import (
	"strconv"
	"testing"

	"gotest.tools/assert"
)

func TestProtocol_computeLRC(t *testing.T) {
	lrc := ComputeLRC("@02 0 00 OK IDLE NI 0:CF")

	assert.Equal(t, strconv.FormatInt(int64(lrc), 16), "cf")
}

func TestProtocol_verifyLRC_validCase(t *testing.T) {
	lrcOk := VerifyLRC("@02 0 00 OK IDLE NI 0:CF", "CF")

	assert.Assert(t, lrcOk)
}
func TestProtocol_verifyLRC_invalidCase(t *testing.T) {
	lrcOk := VerifyLRC("@02 0 00 OK IDLE NI 0:AB", "AB")

	assert.Assert(t, !lrcOk)
}
func TestProtocol_verifyLRC_invalidCase2(t *testing.T) {
	lrcOk := VerifyLRC("@02 0 00 OK IDLE NI 0:AX", "AX")

	assert.Assert(t, !lrcOk)
}
