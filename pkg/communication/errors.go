package communication

type CommErr interface {
	error
	// this methods serves to ensure that only CommErr can be returned from methods
	IsCommErr() bool
}

type ErrConnectionFailed struct {
	message string
}

func NewErrConnectionFailed(message string) CommErr {
	return &ErrConnectionFailed{
		message: message,
	}
}
func (err *ErrConnectionFailed) Error() string {
	return err.message
}
func (err *ErrConnectionFailed) IsCommErr() bool {
	return true
}

type ErrConnectionClosed struct {
}

func NewErrConnectionClosed() CommErr {
	return &ErrConnectionClosed{}
}
func (err *ErrConnectionClosed) Error() string {
	return "Connection has been closed"
}
func (err *ErrConnectionClosed) IsCommErr() bool {
	return true
}

type ErrSerialPortBusy struct {
	message string
}

func NewErrSerialPortBusy(message string) CommErr {
	return &ErrSerialPortBusy{
		message: message,
	}
}
func (err *ErrSerialPortBusy) Error() string {
	return err.message
}
func (err *ErrSerialPortBusy) IsCommErr() bool {
	return true
}
