package communication

import (
	"testing"

	"gotest.tools/assert"
)

func TestPipeName_getsPipeName(t *testing.T) {
	_, err := getPipeName("zaber-message-router", "")
	assert.NilError(t, err)
}
