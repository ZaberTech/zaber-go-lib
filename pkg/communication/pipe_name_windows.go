//go:build windows
// +build windows

package communication

import (
	"fmt"
	"os"
	"sync"
	"syscall"
)

// This method replaces the "os/user" user.current() call to get the UID.
// It's because the "os/user" call is randomly very slow on Windows.
// The code is taken from "os/user/lookup_windows.go".
func getUid() (string, error) {
	t, e := syscall.OpenCurrentProcessToken()
	if e != nil {
		return "", e
	}
	defer t.Close()
	u, e := t.GetTokenUser()
	if e != nil {
		return "", e
	}
	return u.User.Sid.String()
}

func getUidOnce() (string, error) {
	cache.Do(func() { cache.uid, cache.err = getUid() })
	return cache.uid, cache.err
}

var cache struct {
	sync.Once
	uid string
	err error
}

func getPipeName(pipeName, pipeNameEnv string) (string, error) {
	if pipeNameEnv != "" {
		if pipe := os.Getenv(pipeNameEnv); pipe != "" {
			return pipe, nil
		}
	}

	uid, err := getUidOnce()
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("\\\\.\\pipe\\%s-%s", pipeName, uid), nil
}
