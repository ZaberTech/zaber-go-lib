// +build !wasm

package communication

import (
	"bufio"
	"io"
	"log"
	"strings"
	"sync"

	serial "github.com/zabertech/go-serial"
)

type SerialPort struct {
	port serial.Port

	reader     *bufio.Reader
	readerLock sync.Mutex

	writerLock sync.Mutex

	lock     sync.Mutex
	isClosed bool
}

const openSerialErrPrefix = "Cannot open serial port: "
const busySerialPortErrText = "Port is likely already opened by another application. Please close the application that is using the port."

func OpenSerial(portName string, baudRate int) (*SerialPort, CommErr) {
	log.Print("Opening " + portName)

	mode := &serial.Mode{
		BaudRate: baudRate,
	}
	port, err := serial.Open(portName, mode)
	if err != nil {
		if portErr, isPortErr := err.(*serial.PortError); isPortErr {
			switch portErr.Code() {
			case serial.PortBusy:
				return nil, NewErrSerialPortBusy(openSerialErrPrefix + busySerialPortErrText)
			}
		}

		return nil, NewErrConnectionFailed(openSerialErrPrefix + err.Error())
	}

	serialPort := &SerialPort{
		port: port,
	}
	serialPort.reader = bufio.NewReader(port)

	return serialPort, nil
}

func (port *SerialPort) Close() CommErr {
	port.lock.Lock()
	defer port.lock.Unlock()

	if port.isClosed {
		return nil
	}

	port.isClosed = true

	err := port.port.Close()
	if err != nil {
		return NewErrConnectionFailed("Cannot close serial port: " + err.Error())
	}
	return nil
}

func (port *SerialPort) checkReadErrors(err error) CommErr {
	if err != nil {
		if port.IsClosed() {
			return NewErrConnectionClosed()
		}
		return NewErrConnectionFailed("Cannot read from serial port: " + err.Error())
	}

	return nil
}

func (port *SerialPort) ReadLine() (string, CommErr) {
	if port.IsClosed() {
		return "", NewErrConnectionClosed()
	}

	port.readerLock.Lock()
	defer port.readerLock.Unlock()

	line, portErr := port.reader.ReadString('\n')

	err := port.checkReadErrors(portErr)
	if err != nil {
		return "", err
	}

	return strings.TrimSpace(line), nil
}

func (port *SerialPort) Read(num int) ([]byte, CommErr) {
	if port.IsClosed() {
		return nil, NewErrConnectionClosed()
	}

	port.readerLock.Lock()
	defer port.readerLock.Unlock()

	packet := make([]byte, num)
	_, portErr := io.ReadFull(port.reader, packet)

	err := port.checkReadErrors(portErr)
	if err != nil {
		return nil, err
	}

	return packet, nil
}

func (port *SerialPort) WriteLines(lines []string) CommErr {
	if port.IsClosed() {
		return NewErrConnectionClosed()
	}

	port.writerLock.Lock()
	defer port.writerLock.Unlock()

	var sb strings.Builder

	for _, line := range lines {
		sb.WriteString(line)
		sb.WriteString("\n")
	}

	_, err := port.port.Write([]byte(sb.String()))
	if err != nil {
		return NewErrConnectionFailed("Cannot write to serial port: " + err.Error())
	}

	return nil
}

func (port *SerialPort) Write(packet []byte) CommErr {
	if port.IsClosed() {
		return NewErrConnectionClosed()
	}

	port.writerLock.Lock()
	defer port.writerLock.Unlock()

	_, err := port.port.Write(packet)
	if err != nil {
		return NewErrConnectionFailed("Cannot write to serial port: " + err.Error())
	}

	return nil
}

func (port *SerialPort) IsClosed() bool {
	port.lock.Lock()
	defer port.lock.Unlock()

	return port.isClosed
}
