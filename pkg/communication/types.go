package communication

type IO interface {
	ReadLine() (line string, err CommErr)
	Read(num int) ([]byte, CommErr)
	WriteLines([]string) CommErr
	Write([]byte) CommErr
	Close() CommErr
	IsClosed() bool
}
