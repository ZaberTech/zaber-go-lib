zaber-base-lib

Library for various shared Go code in Zaber projects.

Test: `go test -v ./pkg/...`
Lint: `golangci-lint run`
