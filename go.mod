module gitlab.com/ZaberTech/zaber-go-lib

go 1.18

require (
	github.com/zabertech/go-serial v0.0.0-20210201195853-2428148c5139
	gotest.tools v2.2.0+incompatible
)

require (
	github.com/creack/goselect v0.1.2 // indirect
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/sys v0.0.0-20210510120138-977fb7262007 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)
